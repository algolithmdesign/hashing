/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.hashing;
import java.util.Scanner;
/**
 *
 * @author AdMiN
 */

   

public class Hash<Key, Value>{
    int hashSize = 30001;
    Value[] vals = (Value[]) new Object[hashSize];
    Key[] keys = (Key[]) new Object[hashSize];

    private int hash(Key key) {
        return (key.hashCode() & 0x7FFFFFFF) % hashSize;
    }

    public void Put(Key key, Value value) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % hashSize)
            if (keys[i].equals(key))
                break;
        keys[i] = key;
        vals[i] = value;
    }

    public Value Get(Key key){
        for (int i = hash(key); keys[i] != null; i = (i+1) % hashSize){
            if (key.equals(keys[i])){
                return vals[i];
            }
        }
        return null;
    }
    public static void main(String[] args) {
        Hash<String, String> hash = new Hash<>();
        Scanner in = new Scanner(System.in);
        String key = in.next();
        String value = in.next();
        hash.Put(key, value);

        String Finding = in.next();
        if (hash.Get(Finding)==null) {
            System.out.println("not found");
        }else {
            System.out.println(hash.Get(Finding));
        }
    }
}
 

